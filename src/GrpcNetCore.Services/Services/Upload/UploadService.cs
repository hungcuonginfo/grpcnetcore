﻿using System.Threading.Tasks;
using Grpc.Core;
using GrpcNetCore.ServiceDefinition;
using GrpcNetCore.ServiceDefinition.Contracts;
using Microsoft.Extensions.Hosting;

namespace GrpcNetCore.Services.Services.Upload
{
    public class UploadService : IClientStreamingService<UploadRequest, UploadResponse>
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public UploadService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<UploadResponse> Process(IAsyncStreamReader<UploadRequest> requestStream, ServerCallContext context)
        {
            var data = await UploadServiceHelper.GetData(requestStream);
            var filePath = UploadServiceHelper.GetFilePath(_hostingEnvironment.ContentRootPath, data.FileInfo.FileName,data.FileInfo.Extension);
            var result = await UploadServiceHelper.WriteFileFromBytes(data.DataFile, filePath);
            return new UploadResponse {
                Result = "Success"
            };
        }
    }
}
