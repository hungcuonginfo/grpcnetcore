﻿using System.Collections.Generic;
using static GrpcNetCore.ServiceDefinition.UploadRequest.Types;

namespace GrpcNetCore.Services.Services.Upload.ViewModel
{
    public class UploadData
    {
        public FileInfo FileInfo { get; set; }

        public IEnumerable<byte> DataFile { get; set; }

    }
}
