﻿using GrpcNetCore.Services.Services.Upload.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcNetCore.ServiceDefinition;
using System.IO;

namespace GrpcNetCore.Services.Services.Upload
{
    public static class UploadServiceHelper
    {
        public static async Task<UploadData> GetData(IAsyncStreamReader<UploadRequest> requestStream)
        {
            var dataFile = new byte[] { }.AsEnumerable();
            UploadRequest.Types.FileInfo fileInfo;
            while ( await requestStream.MoveNext())
            {
                if (requestStream.Current.FileInfo != null)
                {
                    fileInfo = requestStream.Current.FileInfo;
                    continue;
                }
                if (requestStream.Current.StreamData != null)
                {
                    dataFile = dataFile.Concat(requestStream.Current.StreamData);
                }
            }
            return new UploadData
            {
                FileInfo = requestStream.Current.FileInfo,
                DataFile = dataFile
            };
        }

        public static async Task<bool> WriteFileFromBytes(IEnumerable<byte> dataFile, string filePath) 
        {
            using (var fs = new FileStream(filePath,FileMode.Create,FileAccess.Write))
            {
                await fs.WriteAsync(dataFile.ToArray(), 0, dataFile.Count());
                return true;
            }
        }

        public static string GetFilePath(string contentWebRoot,string fileName, string extension)
        {
            string UploadFileDirectory = "UploadFiles";
            string path = Path.Combine(contentWebRoot, UploadFileDirectory);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string fileNameNew = $"{fileName}-{Guid.NewGuid()}.{extension}";
            return Path.Combine(path,fileNameNew);
        }
    }
}
