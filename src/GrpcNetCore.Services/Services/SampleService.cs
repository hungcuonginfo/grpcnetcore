﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcNetCore.ServiceDefinition;
using GrpcNetCore.ServiceDefinition.Contracts;
using Microsoft.Extensions.Logging;

namespace GrpcNetCore.Services.Services
{
    public class SampleService : Sample.SampleBase
    {
        private readonly ILogger<SampleService> _logger;
        private readonly IClientStreamingService<UploadRequest, UploadResponse> _uploadService;

        public SampleService(ILogger<SampleService> logger,
            IClientStreamingService<UploadRequest, UploadResponse> uploadService)
        {
            _logger = logger;
            _uploadService = uploadService;
        }

        public override Task<UploadResponse> Upload(IAsyncStreamReader<UploadRequest> requestStream, ServerCallContext context)
        {
            return _uploadService.Process(requestStream,context);
        }

        public override Task Download(DownloadRequest request, IServerStreamWriter<DownloadResponse> responseStream, ServerCallContext context)
        {
            return base.Download(request, responseStream, context);
        }

        public override Task<GetFilesResponse> GetFiles(GetFilesRequest request, ServerCallContext context)
        {
            return base.GetFiles(request, context);
        }
    }
}
