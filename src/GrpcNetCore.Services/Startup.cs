﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrpcNetCore.Services.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using GrpcNetCore.ServiceDefinition.Contracts;
using GrpcNetCore.Services;
using GrpcNetCore.ServiceDefinition;
using GrpcNetCore.Services.Services.Upload;
using GrpcNetCore.Services.Services.Download;
using GrpcNetCore.Services.Services.GetFiles;

namespace GrpcNetCore.Services
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc();
            services.AddScoped<Sample.SampleBase,SampleService>();
            services.AddScoped<IClientStreamingService<UploadRequest,UploadResponse>,UploadService>();
            services.AddScoped<IServerStreamingService<DownloadRequest, DownloadResponse>, DownloadService>();
            services.AddScoped<IUnaryService<GetFilesRequest, GetFilesResponse>, GetFilesService>();

            services.AddScoped<IClientStreamingService<UploadRequest, UploadResponse>, UploadService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.Use(async (context, next) => {

                await next();

            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<SampleService>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }
    }
}

