using Grpc.Core;
using System.Threading.Tasks;

namespace GrpcNetCore.ServiceDefinition.Contracts
{
    public interface IUnaryService<TRequest, TResponse>
    {
        Task<TResponse> Process(TRequest request, ServerCallContext context);
    }
}
