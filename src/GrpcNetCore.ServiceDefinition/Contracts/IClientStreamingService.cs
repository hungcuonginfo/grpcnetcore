using Grpc.Core;
using System.Threading.Tasks;

namespace GrpcNetCore.ServiceDefinition.Contracts
{
    public interface IClientStreamingService<TRequest, TResponse>
    {
        Task<TResponse> Process(IAsyncStreamReader<TRequest> requestStream, ServerCallContext context);
    }
}
