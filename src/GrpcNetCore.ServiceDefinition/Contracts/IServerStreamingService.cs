using Grpc.Core;
using System.Threading.Tasks;

namespace GrpcNetCore.ServiceDefinition.Contracts
{
    public interface IServerStreamingService<TRequest, TResponse>
    {
        Task Process(TRequest request, IServerStreamWriter<TResponse> responseStream, ServerCallContext context);
    }
}
