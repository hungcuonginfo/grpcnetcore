﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GrpcNetCore.Mvc.Models;
using Grpc.AspNetCore;
using Grpc.Core;
using Grpc.Net.Client;
using GrpcNetCore.ServiceDefinition;
using System.Net.Http;

namespace GrpcNetCore.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //[HttpPost]
        //public async Task<ActionResult> UploadFile(List<IFormFile> files)
        //{
        //    var httpClient = new HttpClient();
        //    httpClient.BaseAddress = new Uri("http://localhost:5001");

        //    var client= GrpcClient.Create<Sample.SampleClient>(httpClient);
        //    try
        //    {
        //        client.Upload();
        //    }
        //    catch (Exception ex)
        //    { 
            
        //    }
        //}
    }
}
